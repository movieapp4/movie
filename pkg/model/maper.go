package model

import pb "gitlab.com/movieapp4/movie/genproto/movie"

// MetadataToProto converts a Metadata struct into a
// generated proto counterpart.
func MetadataToProtp(m *MetaData) *pb.Metadata {
	return &pb.Metadata{
		Id:          m.ID,
		Title:       m.Title,
		Description: m.Description,
		Director:    m.Director,
	}
}

// MetadataFromProto converts a generated proto counterpart
// into a Metadata struct.
func MetadataFromProto(m *pb.Metadata) *MetaData {
	return &MetaData{
		ID:          m.Id,
		Title:       m.Title,
		Description: m.Description,
		Director:    m.Director,
	}
}