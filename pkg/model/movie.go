package model

// MovieDetails includes movie metadata its aggregated rating.
type MovieDetails struct {
	Rating   float64  `json:"rating"`
	Metadata MetaData `json:"metadata"`
}
