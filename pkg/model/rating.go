package model

// RecotdID defines a record id. Togather with RecordType
// identifies unique records across all types.
type RecordID string

// RecordType defines a record type. Togather with RecotdID
// identifies unique records across all types.
type RecordType string

// Existing record type.
const (
	RecordTypeMovie = RecordType("movie")
)

// UserID defines a user id.
type UserID string

// RatingValue defines a value a rating record.
type RatingValue int

// Rating defines an individual rating created by user for some record.
type Rating struct {
	RecordID   string      `json:"record_id"`
	RecotdType string      `json:"record_type"`
	UserID     UserID      `json:"user_id"`
	Value      RatingValue `json:"value"`
}
