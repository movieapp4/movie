package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/movieapp4/movie/internal/controller/movie"
	metadatagateway "gitlab.com/movieapp4/movie/internal/gateway/metadata/grpc"
	ratinggateway "gitlab.com/movieapp4/movie/internal/gateway/rating/grpc"
	httphandler "gitlab.com/movieapp4/movie/internal/handler/http"
	"gitlab.com/movieapp4/pkg/discovery"
	"gitlab.com/movieapp4/pkg/discovery/consul"
)

const serviceName = "movie"

func main() {
	var port int

	flag.IntVar(&port, "port", 8083, "API handler port")
	flag.Parse()

	log.Printf("Starting the movie service on port %d", port)

	ctx := context.Background()
	instanceID := discovery.GenerateInstanceID(serviceName)

	registry, err := consul.NewRegistry("localhost:8500")
	if err != nil {
		panic(err)
	}

	if err := registry.Register(ctx, instanceID, serviceName, fmt.Sprintf("localhost:%d", port)); err != nil {
		panic(err)
	}

	go func() {
		for {
			if err := registry.ReportHealthyState(ctx, instanceID, serviceName); err != nil {
				log.Panicln("failed to report healthy state: " + err.Error())
			}

			time.Sleep(1 * time.Second)
		}
	}()
	defer registry.Deregister(ctx, instanceID, serviceName)

	metadataGateway := metadatagateway.NewGateway(registry)
	ratingGateway := ratinggateway.NewGateway(registry)
	ctrl := movie.NewController(ratingGateway, metadataGateway)
	h := httphandler.NewHandler(ctrl)

	http.Handle("/movie", http.HandlerFunc(h.GetMovieDetails))

	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil); err != nil {
		panic(err)
	}
}
