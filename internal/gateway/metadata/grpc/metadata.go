package grpc

import (
	"context"

	"gitlab.com/movieapp4/movie/pkg/model"
	"gitlab.com/movieapp4/movie/genproto/movie"
	"gitlab.com/movieapp4/movie/internal/grpcutil"
	"gitlab.com/movieapp4/pkg/discovery"
)

// Gateway defines a movie metadata gRPC gateway.
type Gateway struct {
	resgistry discovery.Registry
}

// New creates a new gRPC gateway for a movie metadata service.
func NewGateway(r discovery.Registry) *Gateway {
	return &Gateway{resgistry: r}
}

// Get returns movie metadata by a movie id.
func (g *Gateway) Get(ctx context.Context, id string) (*model.MetaData, error) {
	conn, err := grpcutil.ServiceConnection(ctx, "metadata", g.resgistry)
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	client := movie.NewMetadataServiceClient(conn)
	resp, err := client.GetMetadata(ctx, &movie.GetMetadataRequest{MovieId: id})
	if err != nil {
		return nil, err
	}

	return model.MetadataFromProto(resp.Metadata), nil
}

