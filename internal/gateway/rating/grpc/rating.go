package grpc

import (
	"context"

	"gitlab.com/movieapp4/movie/genproto/movie"
	"gitlab.com/movieapp4/movie/internal/grpcutil"
	"gitlab.com/movieapp4/movie/pkg/model"
	"gitlab.com/movieapp4/pkg/discovery"
)

// Gateway defines an gRPC gateway for a rating service.
type Gateway struct {
	registry discovery.Registry
}

// New creates a new gRPC gateway for a rating service.
func NewGateway(r discovery.Registry) *Gateway {
	return &Gateway{registry: r}
}

// GetAggregatedRating returns the aggregated rating for a
// record or ErrNotFound if there are no ratings for it.
func (g *Gateway) GetAggregatedRating(ctx context.Context, recordID model.RecordID, recordType model.RecordType) (float64, error) {
	conn, err := grpcutil.ServiceConnection(ctx, "rating", g.registry)
	if err != nil {
		return 0, err
	}
	defer conn.Close()

	client := movie.NewRatingSerivceClient(conn)

	resp, err := client.GetAggregatedRating(ctx, &movie.GetAggregatedRatingRequest{
		RecordId: string(recordID), RecordType: string(recordType),
	})
	if err != nil {
		return 0, err
	}
	return resp.RecordValue, nil
}

func (g *Gateway) PutRating(ctx context.Context,recordID model.RecordID, recordType model.RecordType, rating *model.Rating) error {
	conn, err := grpcutil.ServiceConnection(ctx, "rating", g.registry)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := movie.NewRatingSerivceClient(conn)

	_, err = client.PutRating(ctx, &movie.PutRatingRequest{
		UserId: string(rating.UserID),
		RecordId: string(recordID),
		RecordType: string(recordType),
		RatingValue: int32(rating.Value),
	})
	if err != nil {
		return nil
	}
	return nil
}
